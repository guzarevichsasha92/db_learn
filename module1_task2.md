![img.png](images/img.png)
![img_1.png](images/img_1.png)
![img.png](images/2.png)
# DB_ONE Script

```sql
-- Create the postgres_fdw extension in db_one
CREATE EXTENSION postgres_fdw;

DROP SERVER IF EXISTS same_server_postgres CASCADE;

-- Set up a foreign server in db_one to connect to db_two
CREATE SERVER same_server_postgres
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (dbname 'db_two');

-- Create a user mapping in db_one
CREATE USER MAPPING FOR CURRENT_USER
SERVER same_server_postgres
OPTIONS (USER 'postgres', password '1234');

-- Define a foreign table to access remote_table from db_two
CREATE FOREIGN TABLE local_remote_table (
    id INTEGER,
    name VARCHAR(255),
    age INTEGER
)
SERVER same_server_postgres
OPTIONS (schema_name 'public', table_name 'remote_table');

-- Perform DML operations on the foreign table

-- Select records from the foreign table
SELECT * FROM local_remote_table;

-- Insert a new record into the foreign table
INSERT INTO local_remote_table (id, name, age) VALUES (4, 'Michael Johnson', 30);

-- Update an existing record in the foreign table
UPDATE local_remote_table SET age = 40 WHERE name = 'John Doe';

-- Delete a record from the foreign table
DELETE FROM local_remote_table WHERE name = 'Lucy Brown';

SELECT * FROM local_remote_table;

```

# DB_TWO Script

```sql
-- Create a sample table with some data in db_two
CREATE TABLE remote_table (
    id serial PRIMARY KEY,
    name VARCHAR(255),
    age INTEGER
);

INSERT INTO remote_table (name, age) VALUES
('John Doe', 35),
('Jane Smith', 28),
('Lucy Brown', 42);

```
