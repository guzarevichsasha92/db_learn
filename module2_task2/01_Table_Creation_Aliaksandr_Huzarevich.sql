-- Create the DimEmployees table if it doesn't exist
CREATE TABLE IF NOT EXISTS DimEmployees (
    EmployeeKey SERIAL PRIMARY KEY,
    EmployeeID INT,
    LastName VARCHAR(50),
    FirstName VARCHAR(50),
    Title VARCHAR(50),
    Address VARCHAR(100),
    City VARCHAR(50),
    Region VARCHAR(50),
    PostalCode VARCHAR(20),
    Country VARCHAR(50),
    HireDate DATE,
    EffectiveDate DATE,
    EndDate DATE,
    IsCurrent BOOLEAN
);

-- Create the StagingEmployees table if it doesn't exist
CREATE TABLE IF NOT EXISTS StagingEmployees (
    EmployeeID INT PRIMARY KEY,
    LastName VARCHAR(50),
    FirstName VARCHAR(50),
    Title VARCHAR(50),
    Address VARCHAR(100),
    City VARCHAR(50),
    Region VARCHAR(50),
    PostalCode VARCHAR(20),
    Country VARCHAR(50),
    HireDate DATE
);

-- Step 1: Insert new employees from staging to dimension table
INSERT INTO DimEmployees (
    EmployeeID, LastName, FirstName, Title, Address, City, Region, PostalCode, Country, HireDate, EffectiveDate, EndDate, IsCurrent
)
SELECT
    s.EmployeeID, s.LastName, s.FirstName, s.Title, s.Address, s.City, s.Region, s.PostalCode, s.Country, s.HireDate,
    CURRENT_DATE AS EffectiveDate, NULL AS EndDate, TRUE AS IsCurrent
FROM
    StagingEmployees s
LEFT JOIN
    DimEmployees d
ON
    s.EmployeeID = d.EmployeeID
WHERE
    d.EmployeeID IS NULL;

-- Step 2: Handle updates - end the current record and insert a new record with the changes
-- Update the existing current records to set EndDate and IsCurrent
UPDATE DimEmployees
SET
    EndDate = CURRENT_DATE,
    IsCurrent = FALSE
FROM
    DimEmployees d
JOIN
    StagingEmployees s
ON
    d.EmployeeID = s.EmployeeID
WHERE
    d.IsCurrent = TRUE
    AND (
        d.LastName != s.LastName OR
        d.FirstName != s.FirstName OR
        d.Title != s.Title OR
        d.Address != s.Address OR
        d.City != s.City OR
        d.Region != s.Region OR
        d.PostalCode != s.PostalCode OR
        d.Country != s.Country OR
        d.HireDate != s.HireDate
    );

-- Insert the updated rows as new records
INSERT INTO DimEmployees (
    EmployeeID, LastName, FirstName, Title, Address, City, Region, PostalCode, Country, HireDate, EffectiveDate, EndDate, IsCurrent
)
SELECT
    s.EmployeeID, s.LastName, s.FirstName, s.Title, s.Address, s.City, s.Region, s.PostalCode, s.Country, s.HireDate,
    CURRENT_DATE AS EffectiveDate, NULL AS EndDate, TRUE AS IsCurrent
FROM
    StagingEmployees s
JOIN
    DimEmployees d
ON
    s.EmployeeID = d.EmployeeID
WHERE
    d.IsCurrent = FALSE
    AND (
        d.LastName != s.LastName OR
        d.FirstName != s.FirstName OR
        d.Title != s.Title OR
        d.Address != s.Address OR
        d.City != s.City OR
        d.Region != s.Region OR
        d.PostalCode != s.PostalCode OR
        d.Country != s.Country OR
        d.HireDate != s.HireDate
    );
