-- Loading data into staging tables
INSERT INTO staging_orders SELECT * FROM Orders;
INSERT INTO staging_order_details SELECT * FROM "Order Details";
INSERT INTO staging_products SELECT * FROM Products;
INSERT INTO staging_customers SELECT * FROM Customers;
INSERT INTO staging_employees SELECT * FROM Employees;
INSERT INTO staging_categories SELECT * FROM Categories;
INSERT INTO staging_shippers SELECT * FROM Shippers;
INSERT INTO staging_suppliers SELECT * FROM Suppliers;
