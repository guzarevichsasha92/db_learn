-- Step 7: Validate Data and Perform Reporting Queries

-- Check for record counts in all tables
SELECT 'DimDate' AS Table_Name, COUNT(*) AS Record_Count FROM DimDate
UNION ALL
SELECT 'DimCustomer', COUNT(*) FROM DimCustomer
UNION ALL
SELECT 'DimProduct', COUNT(*) FROM DimProduct
UNION ALL
SELECT 'DimEmployee', COUNT(*) FROM DimEmployee
UNION ALL
SELECT 'DimCategory', COUNT(*) FROM DimCategory
UNION ALL
SELECT 'DimShipper', COUNT(*) FROM DimShipper
UNION ALL
SELECT 'DimSupplier', COUNT(*) FROM DimSupplier
UNION ALL
SELECT 'FactSales', COUNT(*) FROM FactSales
UNION ALL
SELECT 'staging_customers', COUNT(*) FROM staging_customers
UNION ALL
SELECT 'staging_products', COUNT(*) FROM staging_products
UNION ALL
SELECT 'staging_categories', COUNT(*) FROM staging_categories
UNION ALL
SELECT 'staging_employees', COUNT(*) FROM staging_employees
UNION ALL
SELECT 'staging_shippers', COUNT(*) FROM staging_shippers
UNION ALL
SELECT 'staging_suppliers', COUNT(*) FROM staging_suppliers
UNION ALL
SELECT 'staging_order_details', COUNT(*) FROM staging_order_details
UNION ALL
SELECT 'staging_orders_unique_orderdates', COUNT(DISTINCT orderdate) FROM staging_orders;

-- Check for referential integrity in FactSales
SELECT COUNT(*) AS Broken_Record_Count
FROM FactSales
WHERE DateID NOT IN (SELECT DateID FROM DimDate)
   OR CustomerID NOT IN (SELECT CustomerID FROM DimCustomer)
   OR ProductID NOT IN (SELECT ProductID FROM DimProduct)
   OR EmployeeID NOT IN (SELECT EmployeeID FROM DimEmployee)
   OR CategoryID NOT IN (SELECT CategoryID FROM DimCategory)
   OR ShipperID NOT IN (SELECT ShipperID FROM DimShipper)
   OR SupplierID NOT IN (SELECT SupplierID FROM DimSupplier);

-- Display the top five best customers by number of transactions and purchase amount, including region and country information
SELECT
    c.CustomerID,
    c.ContactName,
    c.Region,
    c.Country,
    COUNT(fs.SalesID) AS TotalTransactions,
    SUM(fs.TotalAmount) AS TotalPurchaseAmount
FROM
    FactSales fs
JOIN
    DimCustomer c ON fs.CustomerID = c.CustomerID
GROUP BY
    c.CustomerID,
    c.ContactName,
    c.Region,
    c.Country
ORDER BY
    TotalPurchaseAmount DESC,
    TotalTransactions DESC
LIMIT 5;

-- Display the top five best-selling products by total sales amount and number of transactions
SELECT
    p.ProductID,
    p.ProductName,
    c.CategoryName,
    COUNT(*) AS NumTransactions,
    SUM(fs.TotalAmount) AS TotalSales
FROM
    FactSales fs
JOIN
    DimProduct p ON fs.ProductID = p.ProductID
JOIN
    DimCategory c ON fs.CategoryID = c.CategoryID
GROUP BY
    p.ProductID, p.ProductName, c.CategoryName
ORDER BY
    TotalSales DESC, NumTransactions DESC
LIMIT 5;

-- Display the top five regions by total sales amount
SELECT
    c.Region,
    SUM(fs.TotalAmount) AS TotalSales
FROM
    FactSales fs
JOIN
    DimCustomer c ON fs.CustomerID = c.CustomerID
GROUP BY
    c.Region
ORDER BY
    TotalSales DESC
LIMIT 5;

-- Display the total sales and quantity of items sold for the first week of each month
SELECT
    Month,
    SUM(TotalAmount) AS TotalSalesAmount,
    SUM(QuantitySold) AS TotalQuantitySold
FROM
    FactSales
JOIN
    DimDate ON FactSales.DateID = DimDate.DateID
WHERE
    Day BETWEEN 1 AND 7
GROUP BY
    Month
ORDER BY
    Month;

-- Display a weekly sales report with monthly totals by product category for one year
SELECT
    DP.CategoryID,
    DC.CategoryName,
    EXTRACT(WEEK FROM DD.Date) AS Week,
    EXTRACT(MONTH FROM DD.Date) AS Month,
    SUM(FS.QuantitySold) AS WeeklyQuantitySold,
    SUM(FS.TotalAmount) AS WeeklyTotalAmount,
    SUM(SUM(FS.TotalAmount)) OVER (PARTITION BY EXTRACT(MONTH FROM DD.Date)) AS MonthlyTotalAmount
FROM
    FactSales FS
JOIN
    DimDate DD ON FS.DateID = DD.DateID
JOIN
    DimProduct DP ON FS.ProductID = DP.ProductID
JOIN
    DimCategory DC ON DP.CategoryID = DC.CategoryID
GROUP BY
    DP.CategoryID, DC.CategoryName, EXTRACT(WEEK FROM DD.Date), EXTRACT(MONTH FROM DD.Date)
ORDER BY
    EXTRACT(MONTH FROM DD.Date), EXTRACT(WEEK FROM DD.Date), DP.CategoryID;

-- Display sales rankings by product category with the best-selling categories at the top
SELECT
    DP.CategoryID,
    DC.CategoryName,
    SUM(FS.QuantitySold) AS TotalQuantitySold
FROM
    FactSales FS
JOIN
    DimProduct DP ON FS.ProductID = DP.ProductID
JOIN
    DimCategory DC ON DP.CategoryID = DC.CategoryID
GROUP BY
    DP.CategoryID, DC.CategoryName
ORDER BY
    TotalQuantitySold DESC;

-- Display the median monthly sales value by product category and country
SELECT
    EXTRACT(MONTH FROM d.Date) AS Month,
    p.CategoryID AS ProductCategory,
    c.Country,
    PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY fs.TotalAmount) AS MedianMonthlySales
FROM
    FactSales fs
JOIN
    DimProduct p ON fs.ProductID = p.ProductID
JOIN
    DimCustomer c ON fs.CustomerID = c.CustomerID
JOIN
    DimDate d ON fs.DateID = d.DateID
GROUP BY
    EXTRACT(MONTH FROM d.Date),
    p.CategoryID,
    c.Country
ORDER BY
    Month ASC;
