-- Loading data into fact table
INSERT INTO FactSales (DateID, CustomerID, ProductID, EmployeeID, CategoryID, ShipperID, SupplierID, QuantitySold, UnitPrice, Discount, TotalAmount, TaxAmount)
SELECT
    d.DateID,
    c.CustomerID,
    p.ProductID,
    e.EmployeeID,
    cat.CategoryID,
    s.ShipperID,
    sup.SupplierID,
    od.Quantity,
    od.UnitPrice,
    od.Discount,
    (od.Quantity * od.UnitPrice - od.Discount) AS TotalAmount,
    (od.Quantity * od.UnitPrice - od.Discount) * 0.1 AS TaxAmount
FROM staging_order_details od
JOIN staging_orders o ON od.OrderID = o.OrderID
JOIN staging_customers c ON o.CustomerID = c.CustomerID
JOIN staging_products p ON od.ProductID = p.ProductID
LEFT JOIN staging_employees e ON o.EmployeeID = e.EmployeeID
LEFT JOIN staging_categories cat ON p.CategoryID = cat.CategoryID
LEFT JOIN staging_shippers s ON o.ShipVia = s.ShipperID
LEFT JOIN staging_suppliers sup ON p.SupplierID = sup.SupplierID
LEFT JOIN DimDate d ON o.OrderDate = d.Date;
