-- Combined_Script.sql

-- Step 1: Install Extensions
-- Install pgcrypto extension for data encryption
CREATE EXTENSION IF NOT EXISTS pgcrypto;

-- Install pg_stat_statements extension for performance monitoring
CREATE EXTENSION IF NOT EXISTS pg_stat_statements;

-- Step 2: Verify Shared Libraries
-- Show all shared libraries currently loaded
SELECT name, setting
FROM pg_settings
WHERE name = 'shared_preload_libraries';

-- Step 3: Create the employees Table
-- Create the employees table with an encrypted password column
CREATE TABLE IF NOT EXISTS employees (
    id serial PRIMARY KEY,
    first_name VARCHAR(255),
    last_name VARCHAR(255),
    email VARCHAR(255),
    encrypted_password TEXT
);

-- Step 4: Insert Sample Employee Data
-- Insert sample employee data with encrypted passwords
INSERT INTO employees (first_name, last_name, email, encrypted_password) VALUES
   ('John', 'Doe', 'john.doe@example.com', crypt('password123', gen_salt('bf'))),
   ('Jane', 'Smith', 'jane.smith@example.com', crypt('mypassword', gen_salt('bf')));

-- Step 5: Perform SQL Operations
-- Select all employees
SELECT * FROM employees;

-- Update an employee's last name
UPDATE employees SET last_name = 'Brown' WHERE email = 'jane.smith@example.com';

-- Delete an employee record using the email column
DELETE FROM employees WHERE email = 'john.doe@example.com';

-- Step 6: Query Performance Statistics
-- Gather performance statistics
SELECT * FROM pg_stat_statements ORDER BY calls DESC;

-- Check which queries have the highest average and total runtime
SELECT query, total_exec_time, calls, total_exec_time/calls AS avg_exec_time
FROM pg_stat_statements
ORDER BY total_exec_time DESC;

SELECT * FROM employees;

SELECT * FROM pg_stat_statements ORDER BY calls DESC;

SELECT query, total_exec_time, calls, total_exec_time/calls AS avg_exec_time FROM pg_stat_statements ORDER BY total_exec_time DESC;